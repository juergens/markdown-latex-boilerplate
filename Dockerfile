FROM ubuntu:bionic
LABEL cache_breaker 2018_11_05

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y calibre build-essential

# basic latex
RUN apt-get install -y texlive  texlive-latex-extra texlive-fonts-extra

# local latex
RUN apt-get install -y texlive-lang-german

# custom latex
# gone in bionic and I don't know how to replace. I gues with science
# RUN apt-get install -y texlive-math-extra
RUN apt-get install -y texlive-science

# pandoc in ubuntu-repo is outdated
ENV PANDOC_URL https://github.com/jgm/pandoc/releases/download/2.2.2.1/pandoc-2.2.2.1-1-amd64.deb

RUN apt-get install -y wget sendemail texlive-xetex

RUN TEMP_DEB="$(mktemp)" && \
	wget -O "$TEMP_DEB" "$PANDOC_URL" && \
	dpkg -i "$TEMP_DEB" && \
	rm -f "$TEMP_DEB"

RUN apt-get install -y pandoc-citeproc

# install mermaid
# note: the filter requires a `.puppeteer.json`, that sets --no-sandbox in the working dir of the project
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
RUN apt-get update && apt-get install -y nodejs npm chromium-browser
RUN npm install --global mermaid.cli mermaid-filter
RUN apt-get update && apt-get install -y pandoc

# todo: diese zeile nach oben verschieben
RUN apt-get install -y texlive-bibtex-extra biber
