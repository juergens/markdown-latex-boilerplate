

# how to use this boilerplate

##  syntax examples

Reference: [@Fadda:2008p5482].

    blockquote

inline equation: $1+1 = 2$.

equation:

$$
O_2 + 4e^{-}_{p} + 8H^{+}_{n} \rightarrow 2H_{2}O + 4H^{+}_{p}
$$

\clearpage

figure with markdown:

![This is a caption](images/example.png)

figure with latex:
\begin{figure}
\center{}
\includegraphics{images/example.png}
\caption{This is a caption}
\label{fig:This is a caption}
\end{figure}

\clearpage

table in markdown:

header1 | header2 | header3
---|---|---
a|b|c
d|e|f

table in latex:

\center{
\begin{tabular}{l l l}
\hline
header1 & header2 & header3 \\
\hline
a & b & c \\
d & e & f \\
\hline
\end{tabular}
}

\clearpage
